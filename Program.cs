﻿using System;

namespace Trabalho_Avaliativo_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n1 - Módulo de um número complexo\n2 - Soma de dois números complexos\n3 - Subtração de dois números complexos\n4 - Multiplicação de dois números complexos\n5 - Divisão de dois números complexos\n6 - Sair\nQual opção?");
            int opcao = Convert.ToInt32(Console.ReadLine());
            while (opcao >= 1 && opcao <= 5)
            {
                switch (opcao)
                {
                    case 1:
                        {
                            Console.WriteLine("Informe a parte real do número complexo:");
                            double real = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do número complexo:");
                            double imaginario = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo = new Complexos(real, imaginario);
                            objComplexo.moduloComplexo();
                            break;
                        }

                    case 2:
                        {
                            Console.WriteLine("Informe a parte real do primeiro número complexo:");
                            double real1 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do primeiro número complexo:");
                            double imaginario1 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo1 = new Complexos(real1, imaginario1);
                            Console.WriteLine("Informe a parte real do segundo número complexo:");
                            double real2 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do segundo número complexo:");
                            double imaginario2 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo2 = new Complexos(real2, imaginario2);
                            Complexos resultadoSoma = new Complexos(objComplexo1.somaComplexos(objComplexo2));
                            resultadoSoma.mostraComplexo();
                            break;
                        }

                    case 3:
                        {
                            Console.WriteLine("Informe a parte real do primeiro número complexo:");
                            double real3 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do primeiro número complexo:");
                            double imaginario3 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo3 = new Complexos(real3, imaginario3);
                            Console.WriteLine("Informe a parte real do segundo número complexo:");
                            double real4 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do segundo número complexo:");
                            double imaginario4 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo4 = new Complexos(real4, imaginario4);
                            Complexos resultadoSubtracao = new Complexos(objComplexo3.subtracaoComplexos(objComplexo4));
                            resultadoSubtracao.mostraComplexo();
                            break;
                        }

                    case 4:
                        {
                            Console.WriteLine("Informe a parte real do primeiro número complexo:");
                            double real5 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do primeiro número complexo:");
                            double imaginario5 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo5 = new Complexos(real5, imaginario5);
                            Console.WriteLine("Informe a parte real do segundo número complexo:");
                            double real6 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do segundo número complexo:");
                            double imaginario6 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo6 = new Complexos(real6, imaginario6);
                            Complexos resultadoMultiplicacao = new Complexos(objComplexo5.multiplicacaoComplexos(objComplexo6));
                            resultadoMultiplicacao.mostraComplexo();
                            break;
                        }

                    case 5:
                        {
                            Console.WriteLine("Informe a parte real do primeiro número complexo:");
                            double real7 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do primeiro número complexo:");
                            double imaginario7 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo7 = new Complexos(real7, imaginario7);
                            Console.WriteLine("Informe a parte real do segundo número complexo:");
                            double real8 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Informe a parte imaginária do segundo número complexo:");
                            double imaginario8 = Convert.ToDouble(Console.ReadLine());
                            Complexos objComplexo8 = new Complexos(real8, imaginario8);
                            Complexos resultadoDivisao = new Complexos(objComplexo7.divisaoComplexos(objComplexo8));
                            resultadoDivisao.mostraComplexo();
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }
                Console.WriteLine("\n1 - Módulo de um número complexo\n2 - Soma de dois números complexos\n3 - Subtração de dois números complexos\n4 - Multiplicação de dois números complexos\n5 - Divisão de dois números complexos\n6 - Sair\nQual opção?");
                opcao = Convert.ToInt32(Console.ReadLine());
            }
        }
    }
}
