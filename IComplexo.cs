using System;
using System.Collections.Generic;
using System.Text;

namespace Trabalho_Avaliativo_2
{ 
    interface IComplexo
    {
    void mostraComplexo();
    void moduloComplexo();
    public Complexo somaComplexos(Complexo _nc);
    public Complexo subtracaoComplexos(Complexo _nc);
    public Complexo multiplicacaoComplexos(Complexo _nc);
    public Complexo divisaoComplexos(Complexo _nc);
    }  
}