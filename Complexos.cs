﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trabalho_Avaliativo_2
{
    public class Complexos: IComplexo
    {
        private double parteReal, parteImaginaria;

        /* Construtor 1 - Sem nenhum parâmetro definido*/
        public Complexos()
        {
            parteReal = 0;
            parteImaginaria = 0;
        }

        /* Construtor 2 - Apenas o parâmetro real definido*/
        public Complexos(double _pr)
        {
            parteReal = _pr;
        }

        /* Construtor 3 - Parâmetros real e imaginário definidos*/
        public Complexos(double _pr, double _pi)
        {
            parteReal = _pr;
            parteImaginaria = _pi;
        }

        /* Construtor 4 - Passado como parâmetro o objeto Complexo*/
        public Complexos(Complexos _ncObj)
        {
            parteReal = _ncObj.parteReal;
            parteImaginaria = _ncObj.parteImaginaria;
        }

        public void mostraComplexo()
        {
            if (parteImaginaria < 0)
            {
                Console.WriteLine(parteReal + parteImaginaria + "i");
            }
            else
            {
                Console.WriteLine(parteReal + "+" + parteImaginaria + "i" );
            }
        }

        public void moduloComplexo()
        {
            double modulo;
            modulo = Math.Sqrt(parteReal * parteReal + parteImaginaria * parteImaginaria);
            Console.WriteLine("Módulo do número complexo = " + modulo);
        }

        public Complexos somaComplexos(Complexos _nc)
        {
            Complexos resultadoComplexo = new Complexos();
            resultadoComplexo.parteReal = parteReal + _nc.parteReal;
            resultadoComplexo.parteImaginaria = parteImaginaria + _nc.parteImaginaria;
            return resultadoComplexo;
        }

        public Complexos subtracaoComplexos(Complexos _nc)
        {
            Complexos resultadoComplexo = new Complexos();
            resultadoComplexo.parteReal = parteReal - _nc.parteReal;
            resultadoComplexo.parteImaginaria = parteImaginaria - _nc.parteImaginaria;
            return resultadoComplexo;
        }

        public Complexos multiplicacaoComplexos(Complexos _nc)
        {
            Complexos resultadoComplexo = new Complexos();
            resultadoComplexo.parteReal = parteReal * _nc.parteReal - parteImaginaria * _nc.parteImaginaria;
            resultadoComplexo.parteImaginaria = parteReal * _nc.parteImaginaria - _nc.parteReal * parteImaginaria;
            return resultadoComplexo;
        }

        public Complexos divisaoComplexos(Complexos _nc)
        {
            Complexos resultadoComplexo = new Complexos();
            resultadoComplexo.parteReal = (parteReal * _nc.parteReal + parteImaginaria * _nc.parteImaginaria) / (_nc.parteReal * _nc.parteReal + _nc.parteImaginaria * _nc.parteImaginaria);
            resultadoComplexo.parteImaginaria = (parteImaginaria * _nc.parteReal + parteReal * _nc.parteImaginaria) / (_nc.parteReal * _nc.parteReal + _nc.parteImaginaria * _nc.parteImaginaria);
            return resultadoComplexo;
        }
    }
}
